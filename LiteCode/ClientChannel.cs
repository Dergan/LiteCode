﻿using SecureSocketProtocol2.Network;
using SecureSocketProtocol2.Network.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace LiteCode
{
    public class ClientChannel : Channel
    {
        public ClientChannel()
            : base()
        {

        }

        public override void onChannelClosed()
        {

        }

        public override void onChannelOpen()
        {

        }

        public override void onReceiveData(IMessage message)
        {

        }
    }
}