﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LiteCode
{
    /// <summary>
    /// Improves the speed by using the PacketQueue
    /// </summary>
    public class PacketQueueAttribute : Attribute
    {
        public PacketQueueAttribute()
            : base()
        {

        }
    }
}